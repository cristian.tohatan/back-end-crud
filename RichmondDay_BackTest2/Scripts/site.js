﻿$(document).ready(function () {
    $('.create_section').hide();

    $.ajax({
        type: 'GET',
        url: '/Person/GetList',
        contentType: 'application/json',
        success: function (result) {
            $table.bootstrapTable('load', result);
        }
    });



    $('#btn_create').click(function () {
        $('#btn_create').toggleClass('btn-success');
        $('.create_section').toggle();
    });


    $('#btn_update').click(function () {
        if ($('.selected').length > 0) {
            $('.update_section').toggle();

            var selected = $('.selected');

            var $table = $('#table');
            var selections = $table.bootstrapTable('getSelections');

            $('#u_id').val(selections[0].Id);
            $('#u_first_name').val(selections[0].FirstName);
            $('#u_last_name').val(selections[0].LastName);
            $('#u_email').val(selections[0].Email);
        }

    });

    $('#btn_done_update').click(function () {
        var person = {
            Id: $('#u_id').val(),
            FirstName: $('#u_first_name').val(),
            LastName: $('#u_last_name').val(),
            Email: $('#u_email').val()
        }

        $.ajax({
            type: 'POST',
            url: '/Person/Update',
            data: JSON.stringify(person),
            contentType: 'application/json',
            success: function (result) {
                $table.bootstrapTable('load', result);
                $('.update_section').hide();
                $('#btn_update').removeClass('btn-success');
                $('#btn_delete').removeClass('btn-danger');
            }
        });
    });

    $('#table').on('click-row.bs.table', function (e, row, $element) {
        $('#btn_update').removeClass('btn-success');
        $('#btn_delete').removeClass('btn-danger');
        if ($element.hasClass('selected')) {
            $element.removeClass('selected');
        }
        else {
            $('tr').removeClass('selected');
            $element.addClass('selected');
            $('#btn_update').addClass('btn-success');
            $('#btn_delete').addClass('btn-danger');
        }
    });

    $('#btn_done_create').click(function () {
        var firstName = $('#c_first_name').val();
        var lastName = $('#c_last_name').val();
        var email = $('#c_email').val();

        var person = {
            FirstName: firstName,
            LastName: lastName,
            Email: email
        }

        $.ajax({
            type: 'POST',
            url: '/Person/Create',
            data: JSON.stringify(person),
            contentType: 'application/json',
            success: function (result) {
                $table.bootstrapTable('load', result);
                $('#c_first_name').val('');
                $('#c_last_name').val('');
                $('#c_email').val('');
                $('#create_section').hide();
            }
        });
    });

    $('#btn_confirm_delete').click(function () {
        if ($('.selected').length > 0) {
            var $table = $('#table');
            var selections = $table.bootstrapTable('getSelections');
            var person = selections[0];

            $.ajax({
                type: 'POST',
                url: '/Person/Delete',
                data: JSON.stringify(person),
                contentType: 'application/json',
                success: function (result) {
                    $table.bootstrapTable('load', result);
                }
            });

        }
    });




});


var $table = $('#table');
$(function () {
    var data = [
        {
            "id": 0,
            "name": "Item 0",
            "price": "$0"
        },
        {
            "id": 1,
            "name": "Item 1",
            "price": "$1"
        },
        {
            "id": 2,
            "name": "Item 2",
            "price": "$2"
        },
        {
            "id": 3,
            "name": "Item 3",
            "price": "$3"
        },
        {
            "id": 4,
            "name": "Item 4",
            "price": "$4"
        },
        {
            "id": 5,
            "name": "Item 5",
            "price": "$5"
        }
    ];
    $table.bootstrapTable({ data: data });
});
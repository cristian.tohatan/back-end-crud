﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(RichmondDay_BackTest2.Startup))]
namespace RichmondDay_BackTest2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RichmondDay_BackTest2.Controllers
{
    public class PersonController : Controller
    {
        BackTestEntities db = new BackTestEntities();
        // GET: People
        
        public ActionResult Index()
        {
            return View(db.People.ToList());
        }

        [HttpGet]
        public ActionResult GetList(string url)
        {
            return Json(db.People.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Create(Person person)
        {
            db.People.Add(person);
            db.SaveChanges();
            return Json(db.People.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Update(Person person)
        {
            var personToUpdate = db.People.Find(person.Id);
            if (TryUpdateModel(personToUpdate)){
                db.SaveChanges();
            }
            return Json(db.People.ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(Person person)
        {
            var personToDelete = db.People.Find(person.Id);
            db.People.Remove(personToDelete);
            db.SaveChanges();
            return Json(db.People.ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}